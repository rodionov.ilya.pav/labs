﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Console_Lab4
{
    public class Student
    {
        public string F_I_O {get;set;}
        public string group {get;set;}
        public string exam {get;set;}
        public double score {get;set;}
    }
    public class Vedomost {
        public List<Student> students {get;set;}
        public Vedomost () {
            students = new List<Student>();

            students.Add(new Student{F_I_O = "Родионов Илья Павлович", group = "СПБ", exam = "информатика", score = 4});
            students.Add(new Student{F_I_O = "Родионов Илья Павлович", group = "СПБ", exam = "математика", score = 4});
            students.Add(new Student{F_I_O = "Васильев Роман Андреевич", group = "СИБ", exam = "информатика", score = 3});
            students.Add(new Student{F_I_O = "Васильев Роман Андреевич", group = "СИБ", exam = "математика", score = 3});
            students.Add(new Student{F_I_O = "Бурягин Максим Олегович", group = "СПБ", exam = "информатика", score = 4});
            students.Add(new Student{F_I_O = "Бурягин Максим Олегович", group = "СПБ", exam = "математика", score = 3});
            students.Add(new Student{F_I_O = "Мамедов Джавид Нуриманович", group = "СИБ", exam = "информатика", score = 4});
            students.Add(new Student{F_I_O = "Мамедов Джавид Нуриманович", group = "СИБ", exam = "математика", score = 3});
        }
    }
    public class Program
    {
        static Vedomost vedomost = new Vedomost();
        static void Main(string[] args)
        {
            string exam = "математика"; //переменная экзамена для поиска

            Console.WriteLine("Максимальный средний балл у студентов:");
            for (int i = 0; i < Max_Average_Score_Students().Count(); i++)
                Console.WriteLine(Max_Average_Score_Students()[i]);
                
            Console.WriteLine("Средний балл по " + exam + " = " + Average_Score_Exams(exam).ToString());

            Console.WriteLine("Группа с лучшим средним баллом по " + exam + " - ");

            for (int i = 0; i < Max_Average_Score_Students().Count(); i++)
                Console.WriteLine(Average_Score_Exam_Between_Groups(exam)[i]);
        }
        public static string[] Max_Average_Score_Students() {
            var GroupFIOStudent = from student in vedomost.students
            group student by student.F_I_O into g 
            select new 
            {
                FIO = g.Key,
                TotalScore = from student in g
                let total = student.score
                select total
            };

            double MaxAverageScore = GroupFIOStudent.Max(n=>n.TotalScore.Average());

            var StudentsMaxScoreAverage = from student in GroupFIOStudent
            where student.TotalScore.Average() == MaxAverageScore
            select student.FIO;

            return StudentsMaxScoreAverage.ToArray();
        }
        public static double Average_Score_Exams(string exam) {
            var AverageScoreExamQuery = from student in vedomost.students
            where student.exam == exam.ToLower()
            let totalScore = student.score
            select totalScore;

            return AverageScoreExamQuery.Average();
        }
        public static string[] Average_Score_Exam_Between_Groups(string exam) {
            var GroupStudents = from student in vedomost.students
            group student by student.@group into g 
            select new
            {
                namegroup = g.Key,
                TotalScore = from student in g
                where student.exam == exam
                let total = student.score
                select total

            };

            double MaxAverageScore = GroupStudents.Max(n=>n.TotalScore.Average());

            var MaxAverageScoreGroup = from student in GroupStudents
            where student.TotalScore.Average() == MaxAverageScore
            select student.namegroup;

            return MaxAverageScoreGroup.ToArray();
        }
    }
}
