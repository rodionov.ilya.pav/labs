using System;
using Xunit;

namespace Console_Lab4.Test
{
    public class UnitTest
    {
        [Fact]
        public void Max_Average_Score_Students_Test()
        {
            string[] answer = new string[] {"Родионов Илья Павлович"};
            Assert.Equal(answer.ToString(), Program.Max_Average_Score_Students().ToString());
        }
        [Fact]
        public void Average_Score_Exams_Test()
        {
            double answer = 3.75;
            Assert.Equal(answer, Program.Average_Score_Exams("ИНФОРМАТИКА"));
        }
        [Fact]
        public void Average_Score_Exam_Between_Groups_Test() {
            string[] answer = new string[] {"СПБ"};
            Assert.Equal(answer.ToString(), Program.Average_Score_Exam_Between_Groups("математика").ToString());
        }
    }
}
