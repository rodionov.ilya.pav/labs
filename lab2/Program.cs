﻿using System;

namespace HelloWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Как тебя зовут?");
            var name = Console.ReadLine();
            var currentDate = DateTime.Now;
            Console.WriteLine($"{Environment.NewLine}Привет, {name}, сегодня {currentDate:d}. Время: {currentDate:t}");
            Console.Write($"{Environment.NewLine} Для выхода нажми любую клавишу...");
            Console.ReadKey(true);
        }
    }
}
