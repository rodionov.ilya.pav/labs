﻿using System;

namespace Console_Lab3
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
        public double[] Solve(double a, double b, double c) {
            if (a > -1e-5 && a < 1e-5) return null;
            
            double d = b*b - 4 * a * c;
            if (d < -1e-5) return null;
            if (d > -1e-5 && d < 1e-5) return new double[] {-b / (2 * a), -b / (2 * a)};
            return new double[] {(-b + Math.Sqrt(d)) / (2 * a), (-b - Math.Sqrt(d)) / (2 * a)};
        }
    }
}