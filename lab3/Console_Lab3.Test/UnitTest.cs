using System;
using Xunit;

namespace Console_Lab3.Test
{
    public class UnitTest
    {
        [Fact]
        public void SquareEquation_1_minus2_1_MustHaveOneRoots() 
        {
            Program program = new Program();
            double[] result = program.Solve(1, -2, 1);
            Array.Sort(result);
            Assert.Equal(result[0], 1);
            Assert.Equal(result[0], result[1]);
        }
        [Fact]
        public void SquareEquation_1_0_minus1_MustHaveTwoRoots() 
        {
            Program program = new Program();
            double[] result = program.Solve(1, 0, -1);
            Array.Sort(result);
            Assert.Equal(result[0], -1);
            Assert.Equal(result[1], 1);
        }
        [Fact]
        public void SquareEquation_1_0_1_MustHaveNotRoots() 
        {
            Program program = new Program();
            double[] result = program.Solve(1, 0, 1);
            Assert.Null(result);
        }
        [Fact]
        public void SquareEquation_a_b_c_aNotEqual_0() 
        {
            Program program = new Program();
            double[] result = program.Solve(0, 1, 2);
            Assert.Null(result);
        }
    }
}