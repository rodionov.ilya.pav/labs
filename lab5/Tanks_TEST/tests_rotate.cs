using Tanks_Console;
using Xunit;
using Moq;

namespace Tanks_TEST;

public class tests_rotate
{
    [Fact]
    public void testik1_init()
    {
        //Arrange 
        var m = new Mock<IRotatable>();

        m.SetupGet(mock => mock.Yaw).Returns(0).Verifiable();
        m.SetupGet(mock => mock.Velocity).Returns(90).Verifiable();
        m.SetupSet(mock => mock.Yaw = It.IsAny<double>()).Verifiable();

        RotateObj c = new RotateObj(m.Object);
        //Act
        c.Execute();
        //assert
        m.VerifyAll();
    }
    [Fact]
    public void TeSt2_StartRotate()
    {
        //Arrange 
        var m = new Mock<IRotatable>();

        m.SetupGet(mock => mock.Yaw).Returns(0).Verifiable();
        m.SetupGet(mock => mock.Velocity).Returns(90).Verifiable();

        StartRotate c = new StartRotate(m.Object);
        //Act
        c.Execute();
        //assert
        Assert.NotNull(CmdQueue.Commands[0]);
    }
    [Fact]
    public void Test3_StopRotate()
    {
        var m = new Mock<IRotateStoppable>();
        var injector = new Mock<IInjector>();

        m.Setup(x => x.GetRotateCommand()).Returns(injector.Object).Verifiable();
        injector.Setup(x => x.Inject(It.IsAny<EmptyCommand>())).Verifiable();

        StopRotate c = new StopRotate(m.Object);

        //Act
        c.Execute();

        //Assert
        m.VerifyAll();
    }
    [Fact]
    public void Test4()
    {
        var mock = new Mock<IRotatable>();
        mock.SetupGet(m => m.Yaw).Returns(0).Verifiable();
        mock.SetupGet(m => m.Velocity).Returns(90).Verifiable();

        RotateObj c = new RotateObj(mock.Object);

        c.Execute();

        mock.VerifyAll();
    }
}