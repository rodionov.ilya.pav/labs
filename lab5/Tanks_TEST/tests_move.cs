﻿using System.Numerics;
using Tanks_Console;
using Xunit;
using Moq;

namespace Tanks_TEST;

public class tests_move
{
    [Fact]
    public void testik1_init()
    {
        //Arrange 
        var m = new Mock<IMovable>();

        m.SetupGet(mock => mock.Position).Returns(new Vector2(0, 0)).Verifiable();
        m.SetupGet(mock => mock.Velocity).Returns(new Vector2(2, 3)).Verifiable();
        m.SetupSet(mock => mock.Position = It.Is<Vector2>(v => v.Equals(new Vector2(2, 3)))).Verifiable();

        MoveObj c = new MoveObj(m.Object);
        //Act
        c.Execute();
        //assert
        m.VerifyAll();
    }
    [Fact]
    public void TeSt2_StartMove()
    {
        //Arrange 
        var m = new Mock<IMovable>();

        m.SetupGet(mock => mock.Position).Returns(new Vector2(0, 0)).Verifiable();
        m.SetupGet(mock => mock.Velocity).Returns(new Vector2(2, 3)).Verifiable();

        StartMove c = new StartMove(m.Object);
        //Act
        c.Execute();
        //assert
        Assert.NotNull(CmdQueue.Commands[0]);
    }
    [Fact]
    public void Test3_StopMove()
    {
        var m = new Mock<IMoveStoppable>();
        var injector = new Mock<IInjector>();

        m.Setup(x => x.GetMoveCommand()).Returns(injector.Object).Verifiable();
        injector.Setup(x => x.Inject(It.IsAny<EmptyCommand>())).Verifiable();

        StopMove c = new StopMove(m.Object);

        //Act
        c.Execute();

        //Assert
        m.VerifyAll();
    }
    [Fact]
    public void Test4()
    {
        var m = new Mock<IMovable>();
        m.SetupGet(mock => mock.Position).Returns(new Vector2(0, 0)).Verifiable();
        m.SetupGet(mock => mock.Velocity).Returns(new Vector2(2, 3)).Verifiable();

        MoveObj c = new MoveObj(m.Object);

        c.Execute();

        m.VerifyAll();
    }
}
