﻿namespace Tanks_Console
{
    public interface ICommand
    {
        void Execute();
    }
    public class EmptyCommand : ICommand
    {
        public void Execute() {}
    }
    public interface IInjector
    {
        void Inject(ICommand command);
    }
}