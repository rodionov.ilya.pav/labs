﻿using System.Numerics;

namespace Tanks_Console
{
    public interface IMovable
    {
        Vector2 Position { get; set; }
        Vector2 Velocity { get; set; }
    }
    public interface IMoveStoppable
    {
        IInjector GetMoveCommand();
    }
    public class StartMove : ICommand
    {
        private IMovable m;
        public StartMove(IMovable movable)
        {
            m = movable;
        }
        public void Execute()
        {
            CmdQueue.Commands.Add(new MoveObj(m));
        }
    }
    public class MoveObj : ICommand
    {
        private IMovable m;

        public MoveObj(IMovable movable)
        {
            m = movable;
        }

        public void Execute()
        {
            CmdQueue.Commands.Add(this);
            m.Position += m.Velocity;
        }
    }
    public class StopMove : ICommand
    {
        private IMoveStoppable m;

        public StopMove(IMoveStoppable movable)
        {
            m = movable;
        }

        public void Execute()
        {
            IInjector movecmd = m.GetMoveCommand();
            ICommand emptyCmd = new EmptyCommand();

            movecmd.Inject(emptyCmd);
        }
    }
}
