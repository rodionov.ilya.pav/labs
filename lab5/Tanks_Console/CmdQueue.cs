﻿namespace Tanks_Console
{
    public interface ICmdQueue //удалить
    {
        List<ICommand> Commands();
        void Add();
        void Execute();
    }

    public class CmdQueue : ICommand
    {
        public static List<ICommand> Commands = new List<ICommand>();

        public void Add(ICommand cmd)
        {
            Commands.Add(cmd);
        }
        public void Execute()
        {
            Commands.First().Execute();
            Commands.RemoveAt(0);
        }
    }
}
