﻿namespace Tanks_Console
{
    public interface IRotatable
    {
        double Yaw { get; set; }
        double Velocity { get; set; }
    }
    public interface IRotateStoppable
    {
        IInjector GetRotateCommand();
    }
    public class StartRotate : ICommand
    {
        private IRotatable r;
        public StartRotate(IRotatable rotatable)
        {
            r = rotatable;
        }
        public void Execute()
        {
            CmdQueue.Commands.Add(new RotateObj(r));
        }
    }
    public class RotateObj : ICommand
    {
        private IRotatable r;

        public RotateObj(IRotatable rotatable)
        {
            r = rotatable;
        }

        public void Execute()
        {
            CmdQueue.Commands.Add(this);
            r.Yaw += r.Velocity;
        }
    }
    public class StopRotate : ICommand
    {
        private IRotateStoppable r;

        public StopRotate(IRotateStoppable obj)
        {
            r = obj;
        }

        public void Execute()
        {
            IInjector rotatecmd = r.GetRotateCommand();
            ICommand emptyCmd = new EmptyCommand();

            rotatecmd.Inject(emptyCmd);
        }
    }
}
